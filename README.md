### GeoCoding API APP
***
- Клонуємо репозиторій __git clone__
- Копіюємо __.env.example__ файл у файл __.env__ і прописуємо конфіги БД

#### Обов'язково ``php^7.4``, ``Redis``
```bash
composer install
php artisan key:generate
php artisan migrate
php artisan serve 
```
[API документація](http://127.0.0.1:8000/docs/index.html) 
* для регенерації АПІ документації `php artisan scribe:generate` (зміна домена сервера, на приклад)

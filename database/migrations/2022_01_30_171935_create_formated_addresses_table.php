<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormatedAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formatted_addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('geo_code_id')->unique();
            $table->string('address');
            $table->timestamps();

            $table->foreign('geo_code_id')->references('id')->on('geo_codes')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formatted_addresses');
    }
}

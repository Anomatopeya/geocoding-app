<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressToSubLocalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_to_sub_localities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('address_id');
            $table->unsignedBigInteger('sub_locality_id');
            $table->timestamps();

            $table->foreign('address_id')->references('id')->on('addresses')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('sub_locality_id')->references('id')->on('sub_localities')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_to_sub_localities');
    }
}

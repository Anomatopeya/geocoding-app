<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('geo_code_id')->unique();
            $table->unsignedBigInteger('area_id');
            $table->string('street_number')->nullable();
            $table->text('street_name')->nullable();
            $table->string('postal_code')->nullable();
            $table->timestamps();

            $table->foreign('geo_code_id')->references('id')->on('geo_codes')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('area_id')->references('id')->on('areas')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}

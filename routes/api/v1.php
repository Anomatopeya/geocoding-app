<?php

use App\Http\Controllers\Api\V1\AreaController;
use App\Http\Controllers\Api\V1\GeoCodingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (Request $request) {
    return ['test' => 'ok'];
});

//Route::resource('geo-coding', GeoCoding::class);

Route::post('geo-coding', [GeoCodingController::class, 'store']);
Route::get('geo-coding', [GeoCodingController::class, 'index']);
Route::get('geo-coding/{address}', [GeoCodingController::class, 'show']);

Route::get('area', [AreaController::class, 'index']);
Route::get('area/{area}', [AreaController::class, 'show']);

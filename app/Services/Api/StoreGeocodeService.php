<?php

namespace App\Services\Api;

use App\Models\Address;
use App\Models\AddressToLocality;
use App\Models\AddressToSubLocality;
use App\Models\Area;
use App\Models\Country;
use App\Models\District;
use App\Models\FormattedAddress;
use App\Models\GeoCode;
use App\Models\GeoCodeToCountry;
use App\Models\Locality;
use App\Models\Place;
use App\Models\SubLocality;
use Geocoder\Provider\GoogleMaps\Model\GoogleAddress;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Support\Facades\DB;

class StoreGeocodeService
{
    public const ADMIN_LEVEL_DISTRICT = 2;
    public const ADMIN_LEVEL_PLACE = 3;

    /**
     * @param GoogleAddress $googleAddress
     *
     * @return void
     *@throws \Throwable
     */
    public function storeGeocodeData(GoogleAddress $googleAddress)
    {
        DB::beginTransaction();
        $geoCodeData = $this->storeGeocode($googleAddress);
        if ($geoCodeData['newModel']) {
            $this->storeFormattedAddress($geoCodeData['geoCodeId'], $googleAddress);
            if ($googleAddress->getCountry()) {
                $countryId = $this->storeCountry($googleAddress, $geoCodeData['geoCodeId']);
                $areaId = $this->storeArea($googleAddress, $countryId);
                if ($googleAddress->getAdminLevels()->has(self::ADMIN_LEVEL_DISTRICT)) {
                    $this->storeDistrict($googleAddress, $areaId);
                }
                if ($googleAddress->getAdminLevels()->has(self::ADMIN_LEVEL_PLACE)) {
                    $this->storePlace($googleAddress, $areaId);
                }
                $addressId = $this->storeAddress($googleAddress, $areaId, $geoCodeData['geoCodeId']);
                if ($googleAddress->getLocality()) {
                    $this->storeLocality($googleAddress->getLocality(), $addressId, $countryId);
                }
                if ($googleAddress->getSubLocality()) {
                    $this->storeSubLocality($googleAddress->getSubLocality(), $addressId, $countryId);
                }
            }
        }
        DB::commit();
    }

    /**
     * @param GoogleAddress $address
     *
     * @return array
     */
    protected function storeGeocode(GoogleAddress $address): array
    {
        $newModel = false;
        $model = GeoCode::whereGeoCodeId($address->getId())->firstOrNew();
        if (!$model->geo_code_id) {
            $model->geo_code_id = $address->getId();
            $model->latitude = $address->getCoordinates()->getLatitude();
            $model->longitude = $address->getCoordinates()->getLongitude();
            $model->save();
            $newModel = true;
        }
        return ['geoCodeId' => $model->id, 'newModel' => $newModel];
    }

    /**
     * @param int $geoCodeId
     * @param GoogleAddress $googleAddress
     *
     * @return void
     */
    protected function storeFormattedAddress(int $geoCodeId, GoogleAddress $googleAddress)
    {
        $model = FormattedAddress::whereGeoCodeId($geoCodeId)->firstOrNew();
        if (!$model->id) {
            $model->geo_code_id = $geoCodeId;
            $model->address = $googleAddress->getFormattedAddress();
            $model->save();
        }
    }

    /**
     * @param GoogleAddress $address
     * @param int $geoCodeId
     *
     * @return int
     */
    protected function storeCountry(GoogleAddress $address, int $geoCodeId): int
    {
        $model = Country::whereCode($address->getCountry()->getCode())->firstOrNew();
        if (!$model->code) {
            $model->name = $address->getCountry()->getName();
            $model->code = $address->getCountry()->getCode();
            $model->save();
        }
        GeoCodeToCountry::firstOrCreate(['geo_code_id' => $geoCodeId, 'country_id' => $model->id]);
        return $model->id;
    }

    /**
     * @param GoogleAddress $address
     * @param int $countryId
     *
     * @return int
     */
    protected function storeArea(GoogleAddress $address, int $countryId): int
    {
        $model = Area::where(
            ['country_id' => $countryId, 'code' => $address->getAdminLevels()->first()->getCode()]
        )
            ->firstOrNew();
        if (!$model->code) {
            $model->country_id = $countryId;
            $model->name = $address->getAdminLevels()->first()->getName();
            $model->code = $address->getAdminLevels()->first()->getCode();
            $model->save();
        }
        return $model->id;
    }

    /**
     * @param GoogleAddress $address
     * @param int $areaId
     *
     * @return void
     */
    protected function storeDistrict(GoogleAddress $address, int $areaId)
    {
        $model = District::where(
            ['area_id' => $areaId, 'code' => $address->getAdminLevels()->get(self::ADMIN_LEVEL_DISTRICT)->getCode()]
        )
            ->firstOrNew();
        if (!$model->code) {
            $model->area_id = $areaId;
            $model->name = $address->getAdminLevels()->first()->getName();
            $model->code = $address->getAdminLevels()->first()->getCode();
            $model->save();
        }
    }

    /**
     * @param GoogleAddress $address
     * @param int $areaId
     *
     * @return void
     */
    protected function storePlace(GoogleAddress $address, int $areaId)
    {
        $model = Place::where(
            ['area_id' => $areaId, 'code' => $address->getAdminLevels()->get(self::ADMIN_LEVEL_PLACE)->getCode()]
        )
            ->firstOrNew();
        if (!$model->code) {
            $model->area_id = $areaId;
            $model->name = $address->getAdminLevels()->first()->getName();
            $model->code = $address->getAdminLevels()->first()->getCode();
            $model->save();
        }
    }

    /**
     * @param GoogleAddress $address
     * @param int $areaId
     * @param int $geoCodeId
     *
     * @return int
     */
    protected function storeAddress(GoogleAddress $address, int $areaId, int $geoCodeId): int
    {
        $model = Address::where(
            [
                'geo_code_id' => $geoCodeId
            ]
        )
            ->firstOrNew();
        if (!$model->id) {
            $model->geo_code_id = $geoCodeId;
            $model->area_id = $areaId;
            $model->street_number = $address->getStreetNumber();
            $model->street_name = $address->getStreetName();
            $model->postal_code = $address->getPostalCode();
            $model->save();
        }
        return $model->id;
    }

    /**
     * @param string $localityName
     * @param int $addressId
     * @param int $countryId
     *
     * @return void
     */
    protected function storeLocality(string $localityName, int $addressId, int $countryId)
    {
        $locality = Locality::create(['country_id' => $countryId, 'name' => $localityName]);
        AddressToLocality::create(['locality_id' => $locality->id, 'address_id' => $addressId]);
    }

    /**
     * @param string $subLocalityName
     * @param int $addressId
     * @param int $countryId
     *
     * @return void
     */
    protected function storeSubLocality(string $subLocalityName, int $addressId, int $countryId)
    {
        $subLocality = SubLocality::create(['country_id' => $countryId, 'name' => $subLocalityName]);
        AddressToSubLocality::create(['sub_locality_id' => $subLocality->id, 'address_id' => $addressId]);
    }
}

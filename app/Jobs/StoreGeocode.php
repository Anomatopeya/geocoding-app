<?php

namespace App\Jobs;

use App\Services\Api\StoreGeocodeService;
use Geocoder\Provider\GoogleMaps\Model\GoogleAddress;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreGeocode implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var GoogleAddress
     */
    protected $address;
    /**
     * @var StoreGeocodeService
     */
    protected $service;

    /**
     * Create a new job instance.
     *
     * @param GoogleAddress $address
     *
     * @return void
     */
    public function __construct(GoogleAddress $address)
    {
        $this->address = $address;
        $this->service = new StoreGeocodeService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->service->storeGeocodeData($this->address);
    }
}

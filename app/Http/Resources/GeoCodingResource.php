<?php

namespace App\Http\Resources;

use Geocoder\Provider\GoogleMaps\Model\GoogleAddress;
use Illuminate\Http\Resources\Json\JsonResource;

class GeoCodingResource extends JsonResource
{
    public static $wrap = '';
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /** @var GoogleAddress $this */
        return [
            'success' => true,
            'address' => $this->getFormattedAddress()
        ];
    }
}

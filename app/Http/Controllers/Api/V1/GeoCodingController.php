<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGeoCodeRequest;
use App\Http\Resources\AddressCollection;
use App\Http\Resources\AddressResource;
use App\Http\Resources\GeoCodingResource;
use App\Jobs\StoreGeocode;
use App\Models\Address;
use App\Models\GeoCode;
use App\Services\Api\StoreGeocodeService;
use Geocoder\Laravel\ProviderAndDumperAggregator as Geocoder;

class GeoCodingController extends Controller
{
    /**
     * @return AddressCollection
     */
    public function index(): AddressCollection
    {
        return new AddressCollection(Address::with(['area','locality','subLocality', 'formattedAddress'])->paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreGeoCodeRequest $request
     * @param Geocoder $geocoder
     * @param StoreGeocodeService $service
     *
     * @return GeoCodingResource|\Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(StoreGeoCodeRequest $request, Geocoder $geocoder, StoreGeocodeService $service)
    {
        $geoData = $request->only('latitude', 'longitude');
        $result = $geocoder->reverse($geoData['latitude'], $geoData['longitude'])->get();
        if ($result->first()) {
            if (config('geocoder.queue')) {
                StoreGeocode::dispatch($result->first());
            } else {
                $service->storeGeocodeData($result->first());
            }
            return new GeoCodingResource($result->first());
        } else {
            return response()->json(['success' => false, 'message' => __('api.API error')]);
        }
    }

    /**
     * @param GeoCode $address
     *
     * @return AddressResource
     */
    public function show(GeoCode $address): AddressResource
    {
        return new AddressResource($address);
    }
}

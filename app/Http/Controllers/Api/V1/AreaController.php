<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\AreaCollection;
use App\Http\Resources\AreaResource;
use App\Models\Area;

class AreaController extends Controller
{
    /**
     * @return AreaCollection
     */
    public function index()
    {
        return new AreaCollection(Area::with(['addresses','districts','country','places'])->paginate(5));
    }

    /**
     * @param Area $area
     *
     * @return AreaResource
     */
    public function show(Area $area)
    {
        return new AreaResource($area->with(['addresses','districts','country','places'])->get());
    }
}

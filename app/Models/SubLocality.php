<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SubLocality
 *
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SubLocality newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubLocality newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubLocality query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubLocality whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubLocality whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubLocality whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubLocality whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubLocality whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SubLocality extends Model
{
    use HasFactory;

    protected $fillable = ['country_id', 'name'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AddressToLocality
 *
 * @property int $id
 * @property int $address_id
 * @property int $locality_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToLocality newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToLocality newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToLocality query()
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToLocality whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToLocality whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToLocality whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToLocality whereLocalityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToLocality whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AddressToLocality extends Model
{
    use HasFactory;

    protected $fillable = ['address_id', 'locality_id'];
}

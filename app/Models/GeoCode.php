<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

/**
 * App\Models\GeoCode
 *
 * @property int $id
 * @property string $geo_code_id
 * @property string $latitude
 * @property string $longitude
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCode whereGeoCodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCode whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCode whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCode whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Address|null $address
 * @property-read \App\Models\FormattedAddress|null $formattedAddress
 */
class GeoCode extends Model
{
    use HasFactory;

    protected $with = ['address', 'formattedAddress', 'country'];

    /**
     * @return HasOne
     */
    public function address(): HasOne
    {
        return $this->hasOne(Address::class);
    }

    /**
     * @return HasOne
     */
    public function formattedAddress(): HasOne
    {
        return $this->hasOne(FormattedAddress::class);
    }

    /**
     * @return HasOneThrough
     */
    public function country(): HasOneThrough
    {
        return $this->hasOneThrough(Country::class, GeoCodeToCountry::class, 'geo_code_id', 'id', 'id', 'geo_code_id');
    }
}

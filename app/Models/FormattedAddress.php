<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FormattedAddress
 *
 * @property int $id
 * @property int $geo_code_id
 * @property string $address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FormattedAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormattedAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormattedAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormattedAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormattedAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormattedAddress whereGeoCodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormattedAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormattedAddress whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FormattedAddress extends Model
{
    use HasFactory;
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

/**
 * App\Models\Address
 *
 * @property int $id
 * @property int $geo_code_id
 * @property int $area_id
 * @property string|null $street_number
 * @property string $street_name
 * @property int $postal_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Address newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Address newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Address query()
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereGeoCodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereStreetName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereStreetNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Area $area
 * @property-read \App\Models\Locality|null $locality
 * @property-read \App\Models\SubLocality|null $subLocality
 * @property-read \App\Models\FormattedAddress|null $formattedAddress
 */
class Address extends Model
{
    use HasFactory;

    /**
     * @return BelongsTo
     */
    public function area(): BelongsTo
    {
        return $this->belongsTo(Area::class);
    }

    /**
     * @return HasOneThrough
     */
    public function locality(): HasOneThrough
    {
        return $this->hasOneThrough(Locality::class, AddressToLocality::class, 'locality_id', 'id', 'id', 'address_id');
    }

    /**
     * @return HasOneThrough
     */
    public function subLocality(): HasOneThrough
    {
        return $this->hasOneThrough(
            SubLocality::class,
            AddressToSubLocality::class,
            'sub_locality_id',
            'id',
            'id',
            'address_id'
        );
    }

    /**
     * @return HasOne
     */
    public function formattedAddress(): HasOne
    {
        return $this->hasOne(FormattedAddress::class, 'geo_code_id', 'geo_code_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AddressToSubLocality
 *
 * @property int $id
 * @property int $address_id
 * @property int $sub_locality_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToSubLocality newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToSubLocality newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToSubLocality query()
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToSubLocality whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToSubLocality whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToSubLocality whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToSubLocality whereSubLocalityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AddressToSubLocality whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AddressToSubLocality extends Model
{
    use HasFactory;

    protected $fillable = ['address_id','sub_locality_id'];
}

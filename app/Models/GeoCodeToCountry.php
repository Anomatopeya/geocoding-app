<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GeoCodeToCountry
 *
 * @property int $id
 * @property int $geo_code_id
 * @property int $country_id
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCodeToCountry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCodeToCountry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCodeToCountry query()
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCodeToCountry whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCodeToCountry whereGeoCodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GeoCodeToCountry whereId($value)
 * @mixin \Eloquent
 */
class GeoCodeToCountry extends Model
{
    use HasFactory;

    protected $fillable = ['country_id', 'geo_code_id'];

    public $timestamps = false;
}
